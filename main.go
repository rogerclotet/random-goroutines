package main

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	for {
		fmt.Println("How many digits do you want? (exit: 0)")

		var digits int
		_, err := fmt.Scanf("%d", &digits)
		if err != nil {
			fmt.Printf("The input wasn't a number, try again please.")
			continue
		}

		if digits == 0 {
			return
		}

		digitsChannel := make(chan int)

		ctx, cancel := context.WithCancel(context.Background())
		for i := 0; i < digits; i++ {
			go generateRandomDigits(ctx, digitsChannel)
		}

		go consumeAndDisplayDigits(ctx, digitsChannel, digits)

		sigs := make(chan os.Signal)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

		<-sigs

		cancel()
	}
}

func generateRandomDigits(ctx context.Context, digits chan<- int) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
			digits <- rand.Intn(10)
		}

		time.Sleep(time.Millisecond * time.Duration(rand.Intn(1000)))
	}
}

func consumeAndDisplayDigits(ctx context.Context, digits <-chan int, amount int) {
	digitsSlice := make(digitSlice, amount)

	for {
		select {
		case digit := <-digits:
			pos := rand.Intn(amount)
			digitsSlice[pos] = digit
			fmt.Println(digitsSlice)
		case <-ctx.Done():
			return
		}
	}
}

type digitSlice []int

func (d digitSlice) String() string {
	result := ""
	for _, digit := range d {
		result += fmt.Sprintf("%d", digit)
	}
	return result
}
